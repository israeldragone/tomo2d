#!/bin/bash

#input=$1

[ $# -lt 1 ] && ls ../../../vel_models/*.grd && echo "Type the name of input grd grid" && read input

echo "calculate 1-tele or 2-local travel time (1/2)"
read opt


if [ $opt -eq 1 ]; then


	##calculate time for each segments of ray dividing raypath per velocity model (tele)

	for i in ../../../rays2/tele/* 
	do
		out=`echo $i | awk -F "ray" '{print "time"$3}'`
		grdmath ${i} ${input} DIV = ${out} -V

	done

	### sum all points on grid generating travel time
	echo "Calculating travel local time..."

	> times.dat

	for i in time*grd 
	do

		time=`grd2xyz ${i} | gmtmath STDIN SUM = | tac | head -n1 | awk '{printf "%.3f", $3}'`
		echo $i | awk -v t=$time -F"[_,.]" '{print $2,$3,$4,t}' >> times.dat


	done



	echo "Done"

elif [ $opt -eq 2 ]; then


	for i in ../../../rays2/local/* 
	do
		out=`echo $i | awk -F "ray" '{print "time"$3}'`
		grdmath ${i} ${input} DIV = ${out} -V

	done

	### sum all points on grid generating travel time
	echo "Calculating travel local time..."

	> times.dat

	for i in time*grd 
	do

		time=`grd2xyz ${i} | gmtmath STDIN SUM = | tac | head -n1 | awk '{printf "%.3f", $3}'`
		echo $i | awk -v t=$time -F"[_,.]" '{print $2,$3,$4,t}' >> times.dat


	done




	echo "Done"

fi



