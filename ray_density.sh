#!/bin/bash

echo "Generate ray density to 1-small(-R0/100/-20/0) or 2-large(-R0/300/-90/0) model"
read opt

if [ $opt -eq 1 ]; then

	if [ ! -s rays/ray_density.grd ]; then

		#create a grid of zeros
		grd2xyz rays/tele/ray_000_000_020.grd | awk '{print $1,$2,0}' | xyz2grd -R-R0/100/-20/0 -I1/1 -Grays/aux.grd

		##Loop for all rays summinm then
		for i in rays/*/ray_*
		do
			grdmath $i rays/aux.grd ADD = rays/tmp.grd
			mv rays/tmp.grd rays/aux.grd
		done

		mv rays/aux.grd rays/ray_density.grd

	fi

	grdimage rays/ray_density.grd -R0/100/-20/0 -JX25/5 -Ccopper -B10/5 > rays/ray_density.ps
	psconvert rays/ray_density.ps -A -P -Tg
	eog rays/ray_density.png


elif [ $opt -eq 2 ]; then

	if [ ! -s rays2/ray_density.grd ]; then

		#create a grid of zeros
		grd2xyz rays2/tele/ray_000_000_020.grd | awk '{print $1,$2,0}' | xyz2grd -R-R0/100/-20/0 -I1/1 -Grays/aux.grd

		##Loop for all rays summinm then
		for i in rays2/*/ray_*
		do
			grdmath $i rays/aux.grd ADD = rays2/tmp.grd
			mv rays2/tmp.grd rays/aux.grd
		done

		mv rays2/aux.grd rays/ray2_density.grd

	fi

	grdimage rays2/ray2_density.grd -R0/300/-90/0 -JX25/5 -Ccopper -B25/10 > rays2/ray2_density.ps
	psconvert rays2/ray2_density.ps -A -P -Tg
	eog rays2/ray2_density.png

fi
