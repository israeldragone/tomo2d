#!/bin/bash 

input=$1

[ $# -lt 1 ] && echo "Type the name of input tiff grid" && read input


##select gmt5
gogmt gmt5

##convert from tiff to grd
grdconvert $input tmp.grd

##insert -R dimensions in head
grdedit -R0/150/-45/0 tmp.grd

##resample grid increment to 1/1
grdsample tmp.grd -I2/2 -nn+c -T -Goutput.grd

##show the interpolate grid
grdimage output.grd -R0/150/-45/0 -Cgray -B50/10 -JX25/5 > tmp.ps
psconvert tmp.ps -A -Tg -P
eog tmp.png

####correct bad interpolation
#grd2xyz output.grd | awk '{if($3<90) print $1,$2,0; else if ($3>160) print $1,$2,255; else print $0}' | xyz2grd -R0/300/-90/0 -I1/1 -Gcorrected.grd


###change the color value(0,127,255) to velocity value
grd2xyz output.grd | awk '{if($3==127) print $1,$2,6.4; else if ($3==0) print $1,$2,6.592; else print $1,$2,6.208}' | xyz2grd -R0/150/-45/0 -I2/2 -Goutput_vel.grd
