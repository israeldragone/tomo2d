#!/bin/bash


echo "Generate 1-small(-R0/100/-20/0) or 2-large(-R0/150/-45/0) model"
read opt

if [ $opt -eq 1 ]; then

	mkdir -pv rays/local
	mkdir -pv rays/tele


	##generating rays tele
	for i in {000..100..5}; do for j in {000..100..2}; do project -N -C$j/-20 -E$i/0 -G0.001 | tac | awk 'NR > 1 {print $1,$2,1}' | blockmean  -R0/100/-20/0 -I1/1 -C -Ss | awk '{print $1,$2,$3*0.001}' | xyz2grd -R0/100/-20/0 -I1/1 -G"rays/tele/ray_${i}_${j}_020.grd" -V; done;done

	###generating rays local
	for i in {000..100..5}; do for k in {000..100..100}; do for j in {002..18..2}; do project -N -C$k/-$j -E$i/0 -G0.001 | tac | awk 'NR > 1 {print $1,$2,1}' | blockmean  -R0/100/-20/0 -I1/1 -C -Ss | awk '{print $1,$2,$3*0.001}' | xyz2grd -R0/100/-20/0 -I1/1 -G"rays/local/ray_${i}_${k}_${j}.grd" -V; done;done;done

elif [ $opt -eq 2 ]; then


	mkdir -pv rays2/local
	mkdir -pv rays2/tele


##generating rays tele with incidet anngle between 12.5 (i=j-10) and 28 (i=j-24) degress  
	for i in $(seq 000 10 150)
	do 
		if [ $[i-24] -lt 0 ]; then 
			start=0
		else
			start=$[i-24]
		fi

		if [ $[i+24] -gt 150 ]; then 
			end=150
		else
			end=$[i+24]
		fi

		for j in $(seq $start 2 $end)
		do 
			dif=$[i-j]

			if [ ${dif#-} -ge 4 ]; then

				auxi=`printf "%03d" $i`
				auxj=`printf "%03d" $j`

				project -N -C$j/-45 -E$i/0 -G0.001 | tac | awk 'NR > 1 {print $1,$2,1}' | blockmean  -R0/150/-45/0 -I2/2 -C -Ss | awk '{print $1,$2,$3*0.001}' | xyz2grd -R0/150/-45/0 -I2/2 -G"rays2/tele/ray_${auxi}_${auxj}_045.grd" -V

			fi

		 done

	done

	#generating rays local
	for i in {000..150..10}; do for k in {000..150..150}; do for j in {002..030..2}; do project -N -C$k/-$j -E$i/0 -G0.001 | tac | awk 'NR > 1 {print $1,$2,1}' | blockmean  -R0/150/-45/0 -I2/2 -C -Ss | awk '{print $1,$2,$3*0.001}' | xyz2grd -R0/150/-45/0 -I2/2 -G"rays2/local/ray_${i}_${k}_${j}.grd" -V; done;done;done



else

	echo "Bad choice"

fi
