#!/usr/local/anaconda2/bin/python

from __future__ import division, print_function

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from netCDF4 import Dataset
import scipy.sparse.linalg as la
from scipy.sparse import csc_matrix
import glob,sys,time

### set params ####

plt.rcParams.update({'font.size': 16})



def help():

	print(" ")
	print ("-ray to draw ray tracing")
	print ("-inv to inverse velocity model")
	print ("-inv2 to smoothed inverse velocity model")
	print ("-rplot plot initial velocity model")
	print ("-invplot plot inverted velocity model")
	print ("-splot to compare initial and inverse velocity model")
	print ("-cmodel to change rays files choice")
	print ("-checkt to compare real time with time estimated")
	print ("-checkv to compare real vel with vel calc")
	print ("-hc to hitcount map")
	print ("-exit finalize execution")

	print(" ")
	opt = raw_input("Choose an option:\n")

	return opt

def chooseFiles(velmodel):

	'''
	Ask if will be used local, tele or both events
	'''
	##based on velocity model choosen decide if the area to be used is the small (ray) or large (ray2)

	if ("large" in velmodel): 
		ray_folder = "rays2"
	else:
		ray_folder = "rays"


	answ = int(raw_input("Choose the rays to be used\n\n1-both\n2-local\n3-tele\n"))

	while (answ not in {1,2,3}):

		print("Invalid choice\n")
		answ = raw_input("Choose the rays to be used\n\n1-both\n2-local\n3-tele\n")

	if (answ == 1):
		files = sorted(glob.glob(ray_folder+"/tele/ray_*")) + sorted(glob.glob(ray_folder+"/local/ray_*"))

	elif (answ == 2):
		files = sorted(glob.glob(ray_folder+"/local/ray_*"))
	else:
		files = sorted(glob.glob(ray_folder+"/tele/ray_*"))


	return files,answ


def chooseVelModel():
	'''
	Loop to user choose velocity model
	'''
	models = sorted(glob.glob("vel_models/*.grd"))

	for i in range(len(models)):
		print ("%d - %s" % (i+1,models[i]))

	index = raw_input("\nType the number of the desired model\n")

	while (int(index)-1 > len(models)): 
		print("Invalid choice, type the number again")
		index = raw_input()

	print()

	return (models[int(index)-1],int(index), len(models))


def chooseTtime(v_choice,source_type,nmodels):

	'''
	Select travel time based on the vel model and sources (local/tele/both) choosen
	'''

	#create a list with all travel time files alphabetic sorted
	models = sorted(glob.glob("travel_time/times*.dat"))

	## in order will appear 1-both sources, 2-local and 3-tele represented in source_type
	## v_choice represent the alphabetic order velocity model choosen
	## so the correct travel time file is in the position nmodels(source_type-1)+(v_choice-1) of models


	ttime = readTtime(models[nmodels*(source_type-1)+(v_choice-1)])



	return (ttime)


def readTtime(filename):

	'''
	Read travel time file that is in the format where each line is one ray given by:
	Xrec Xsrc Ysrc ttime
	'''

	return (np.loadtxt(filename, unpack=True)[3])


def readRays(files):
	'''
	-read rays grid file one by one
	-convert to 1d array
	-change NaN values to zero
	-return 2 dimensional array where each line is a grid read
	'''
	rays=[]
	for filename in files:
		data = extractGrid(filename)
		ray = convertRay(data)
		ray[np.isnan(ray)] = 0 
		rays.append(ray)

	return (np.array(rays))


def readRaysName(filename):
	'''
	files are in format rays/local|tele/ray_XRC_XSR_YSR.grd where XRC marks the X receiver position and
	XSR is X source positions and YSR Y source both with 3 digits
	All receivers are in y=0
	As all information is in filename we cut off the path name using filename.split("/")[2] and
	extract de information the information as follow
	
	0 1 2 3 4 5 6 7 8 9 10 11 12 13 14
	r a y _ X R C _ X S R  _  Y  S  R .grd
	'''
	xr=int(filename.split("/")[2][4:7])
	yr=0
	xs=int(filename.split("/")[2][8:11])
	ys=int(filename.split("/")[2][12:15])

	return ((xs,xr),(-ys,yr))

def preparePlotRays(files, sel):

	aux = np.array(files)
	c_files = aux[(sel)].tolist()

	return (c_files)


def plotRays(files):
	'''
	Make a plot of ray tracing
	'''
	plt.figure(figsize=(9,3.2))

	for filename in files:
		x,y = readRaysName(filename)
		plt.plot(x[0:2],y[0:2], 'k-')

	plt.xlim([0,150])
	plt.ylim([0,-45])
	plt.yticks([0,-15,-30,-45])
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")
	plt.gca().invert_yaxis()
	plt.tight_layout()

	plt.show()

	return 0

def hitCount(rays,zcells,xcells):
	'''
	Hit count calculation by sum all coluns of ray matrix and dividing by the size of cel (4)
	'''
	ray_dens_v = np.sum(rays,axis=0)
	ray_dens = ray_dens_v.reshape(zcells,xcells) / np.max(ray_dens_v)

	cmap = plt.cm.afmhot

	plt.figure(figsize=(9,4.5))
	plt.imshow(ray_dens, cmap=cmap, extent=[0,150,0,-45])
	plt.yticks([0,-15,-30,-45])
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")
	plt.gca().invert_yaxis()

	plt.colorbar(label='nomalized ray density',cmap=cmap,ticks=[0.0,0.5,1,0], orientation="horizontal")
	plt.clim(0, 1)
	plt.tight_layout(rect=[0,0,1,1.1])
	plt.show()

	return 0

def extractGrid(filename):
	'''
	Extract grid from gmt format to array
	'''

	grid = Dataset(filename)
	data = {"x": grid.variables["x"][:], "y": grid.variables["y"][:], "z": grid.variables["z"][:]}
	return (data["z"])

def plotTime(ttimes,times):
	'''
	Plot real time vales and times after add noise
	'''
	plt.plot(ttimes,'r',lw=3,label="original")
	plt.plot(times,'k',label="noise")
	plt.plot(np.abs(times-ttimes),'b',label="|dif|")
	plt.legend()
	plt.show()

	return 0

def plotOneModel(ini_model):
	'''
	Plot ini vel model
	'''

	# make a color map of fixed colors
	cmap=plt.cm.get_cmap("seismic_r")
#	cmap = plt.cm.seismic

	plt.figure(figsize=(9,4.5))
	plt.imshow(ini_model, cmap=cmap, extent=[0,150,0,-45])
	plt.gca().invert_yaxis()
	plt.yticks([0,-15,-30,-45])
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")

	# make a color bar
	plt.colorbar(label='P velocity (km/s)',cmap=cmap,ticks=[6.2,6.4,6.6], orientation="horizontal")
	plt.clim(6.2, 6.6)
	plt.tight_layout(rect=[0,0,1,1.1])


	plt.show()

	return 0

def compareGrid(ini_model,inv_model):
	'''
	Plot synthetic grid and inveted grid
	'''

	# make a color map of fixed colors
	cmap = plt.cm.get_cmap("seismic_r")

	plt.subplot(211)
	plt.title("Real model")
	plt.imshow(ini_model, cmap=cmap, extent=[0,150,0,-45])
	plt.gca().invert_yaxis()
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")

	plt.subplot(212)
	plt.title("inv model")
	plt.imshow(inv_model, cmap=cmap, extent=[0,150,0,-45])
	plt.gca().invert_yaxis()
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")

	# make a color bar
	plt.colorbar(label='P velocity (km/s)',cmap=cmap,ticks=[6.2,6.4,6.6], orientation="horizontal")
	plt.clim(6.2, 6.6)

	plt.tight_layout(pad=0.1, w_pad=0.0, h_pad=0.1)

	plt.show()

	return 0

def convertRay(data):

	'''
	Transform 2darray in 1d array
	'''
	vec2d = np.array(data)
	vec1d = vec2d.reshape(vec2d.shape[0]*vec2d.shape[1])

	return (vec1d)

def array2grid(slowness,velmodel):

	'''
	Convert 1d slowness array to 2d velocity grid
	'''

	ones=np.ones(slowness.shape)
	velg = (ones/slowness).reshape(velmodel.shape[0],velmodel.shape[1])

	return (velg)


def createSmoothMatrix(length, aux):

	print (length, aux)

	D=np.zeros((length,length))

	##set derivation on x (dx)

	##set first line
	D[0][0]=-1
	D[0][1]=1

	for i in range(1,length):

		if (i%aux == 0 or (i+1)%aux == 0): 
			D[i][i]=-1
			D[i][i-1]=1

		else:
			D[i][i]=-2
			D[i][i+1]=1
			D[i][i-1]=1


	##set derivation on z (dz)


	for i in range(length):

		if (i < aux):
			D[i][i]+=-1
			D[i][i+aux]+=1

		elif (length - i <= aux):
			D[i][i]+=-1
			D[i][i-aux]+=1

		else:
			D[i][i]+=-2
			D[i][i+aux]+=1
			D[i][i-aux]+=1

	return D

def addNoise(factor, time):
	'''
	Add a guassian normalized noise to time array weighted by a user choosen factor
	'''

	### gaussian noise
	gauss = np.random.normal(0,3,np.size(time))

	### normalize vector
	n_gauss = gauss/np.max(gauss)

	### noise is a product of each value by a randon number between -1 and 1 multiplyed by factor
	noise = factor*n_gauss

	plt.plot(n_gauss)
	plt.show()

	print(np.max(noise),np.max(time))

	return (time+noise)


def chooseRays(rays,ttimes,perc):

	N=int(perc*np.size(ttimes))
	pos_selected = sorted(np.random.choice(np.size(ttimes),N,replace=False))

	sel_rays = rays[(pos_selected),:]
	sel_ttimes = ttimes[(pos_selected)]

	return (sel_rays, sel_ttimes, pos_selected)
 

def velocityInv(rays, ttimes):
	'''
	Ax=b -> rays * slowness = ttimes
	min ||Ax-b||2 

	setting up a vector x0 of aproximated velocities we have:
	r0 = b - A*x0 as the residual time, then we solve
	A*dx = r0 and find the solutions as:
	x = x0 + dx
	'''

	slow0=(np.ones(rays.shape[1])/6.4) ## setting vel0 with  slowness of 1/6.4 in the necessary shape
	r0 = ttimes - rays.dot(slow0)  ## calculate the residual time

#	ds = la.lsqr(rays,r0,damp=0.0, atol=1e-15, btol=1e-15, conlim=100000000.0, iter_lim=None, show=True)
	ds = la.lsmr(rays,r0, damp=1.5, atol=1e-8, btol=1e-8, conlim=100000000.0, maxiter=None,show=True)


	slowness = slow0 + ds[0]

	return (slowness)


def velocityInv2(rays, ttimes, xcells):
	'''
	Ax=b -> rays * slowness = ttimes
	min ||Ax-b||2 

	setting up a vector x0 of aproximated velocities we have:
	r0 = b - A*x0 as the residual time, then we solve
	A*dx = r0 and find the solutions as:

	To impose a spacial smoothing we minimize the horizontal second difference
	creating the D matrix thus we will minimize J(x), where:

	J(x0) =  ||Ax0-b||2 + alpha||Dx0||

	After calculate de derivative of J(x0) and equalizing to 0, the min solutions is:

	x0 = (A^T*A + alpha*D^T*D)^-1 A^T*b

	Calling G = (A^T*A + alpha*D^T*D)
	and m = A^T*b, we have:

	x0 = G^-1*m that we can solve by least sqare and then

	x = x0 + dx is the final solution
	'''

	print("%d cells and %d rays" % (rays.shape[1], rays.shape[0]))
	time.sleep(1)

	slow0=(np.ones(rays.shape[1])/6.4) ## setting vel0 with  slowness of 1/6.4 in the necessary shape
	r0 = ttimes - rays.dot(slow0)  ## calculate the residual time

#	Create matrix of smoothing (D)
	D = createSmoothMatrix(rays.shape[1],xcells)

	#set smooth
	alpha = float(raw_input("Type smooth value \n"))
#	alpha = 2

	A=csc_matrix(rays)
	G1=A.transpose().dot(A)
	print("Generated G1")

	D1=csc_matrix(D)  ## G matrix
	G2=D1.transpose().dot(D1)
	print("Generated G2")


	G = csc_matrix(G1 + alpha*G2)
	print("Generated G")

	m = A.transpose().dot(r0)
	print("Generated m")


	print("starting inv")
#	ds = la.lsmr(G,m, damp=0.0, atol=1e-8, btol=1e-8, conlim=100000000.0, maxiter=20000,show=True)
#	ds = la.lsqr(G,m, damp=0.0, atol=1e-7, btol=1e-7, conlim=100000000.0, iter_lim=30000,show=True)
#	ds = la.qmr(G, m, x0=None, tol=1e-10, maxiter=30000, M1=None, M2=None, callback=None)
#	ds = la.cg(G, m, x0=None, tol=1e-08, maxiter=None, M=None, callback=None)
#	ds = la.bicg(G, m, x0=None, tol=1e-08, maxiter=None, M=None, callback=None)
#	ds = la.minres(G, m, x0=None, shift=0.0, tol=1e-8, maxiter=30000, M=None, callback=None, show=True, check=False)
	ds = la.lgmres(G, m, x0=None, tol=1e-8, maxiter=1000, M=None, callback=None, inner_m=30, outer_k=3, outer_v=None, store_outer_Av=True)

	slowness = slow0 + ds[0]

	return (slowness)



if __name__ == "__main__":

	if (len(sys.argv) < 2): 
		opt = help()
	else:
		opt = sys.argv[1]

	##vel model select by user
	velmodel,v_choice, nmodels = chooseVelModel()

	##create file list aplphabetic sorted
	files,source_type = chooseFiles(velmodel)
	c_files = files

	##read lenght of velocity grid
	zcells = extractGrid(velmodel).shape[0]
	xcells = extractGrid(velmodel).shape[1]

	##create traveltime array
	ttimes = chooseTtime(v_choice,source_type,nmodels)

	##create rays gridmatrix
	rays = readRays(files)

	##filter rays
	yn = raw_input("filter rays? (y/n)\n")

	if (yn == "y"):
		perc = raw_input("What percent of rays fo you wanna use? (0 - 1)\n")
		rays, ttimes, sel = chooseRays(rays,ttimes,float(perc))
		c_files = preparePlotRays(files,sel)


	while (opt != "-exit" and opt != "exit"):

		if (opt == "-ray" or opt == "ray"):

			##print ray tracing
			plotRays(c_files)

		elif (opt == "-cmodel" or opt == "cmodel"):

			##vel model select by user
			velmodel,v_choice, nmodels = chooseVelModel()

			##create file list aplphabetic sorted
			files,source_type = chooseFiles(velmodel)
			c_files = files

			##read lenght of velocity grid
			zcells = extractGrid(velmodel).shape[0]
			xcells = extractGrid(velmodel).shape[1]

			##create traveltime array
			ttimes = chooseTtime(v_choice,source_type,nmodels)

			##create rays gridmatrix
			rays = readRays(files)

			##filter rays
			yn = raw_input("filter rays? (y/n)\n")

			if (yn == "y"):
				perc = raw_input("What percent of rays fo you wanna use? (0 - 1)\n")
				rays, ttimes, sel = chooseRays(rays,ttimes,float(perc))
				c_files = preparePlotRays(files,sel)

		elif ("inv" in opt):

			##add noise
			put_noise = raw_input("Add noise to time array? (y/n)\n")

			if ( put_noise == "y" ):
				factor = raw_input ("What percent? 0 - 1\n")
				times = addNoise(float(factor),ttimes)
			else:
				times = ttimes

#			plotTime(ttimes,times)

			##inv slowness model
			if ("inv2" in opt):
				slowness = velocityInv2(rays, times, xcells)
			else:
				slowness = velocityInv(rays, times)

			##turn the 1d array slowness to velocity grid
			vel_grid = array2grid(slowness, extractGrid(velmodel))

		elif (opt == "-rplot" or opt == "rplot"):

			##plot initial and inverted grid
			plotOneModel(extractGrid(velmodel))

		elif (opt == "-invplot" or opt == "invplot"):

			##plot initial and inverted grid
			plotOneModel(vel_grid)

		elif (opt == "-splot" or opt == "splot"):

			##plot initial and inverted grid
			compareGrid(extractGrid(velmodel),vel_grid)

		elif (opt == "-hc" or opt == "hc"):

			##plot hitcount
			hitCount(rays,zcells,xcells)

		elif (opt == "-checkt" or opt == "checkt"):

		## compare ttimes generated in shell with the calculation here after read and convert data
			for i in range(len(ttimes)):
				print("%.3f %.3f %.3f" % (np.sum(rays[i]/convertRay(extractGrid(velmodel))),np.sum(rays[i]*slowness),ttimes[i]))

		elif (opt == "-checkv" or opt == "checkv"):

			slowness[slowness < 0.0001] = 0.0001 ## set 1 to all values lower than 0.01 to calculate 1/slowness
			ones=np.ones(len(slowness))
			vinv = ones/slowness

			inivel = convertRay(extractGrid(velmodel))

			for i in range(len(slowness)):
				print("%.3f %.3f %.2f%%" % (vinv[i],inivel[i],100*(vinv[i]-inivel[i])/inivel[i]))

		elif(opt == "-exit" or opt == "exit"):
			break

		else:

			print("Bad option")
			opt = help()

		opt = help()




